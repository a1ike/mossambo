"use strict";

function hideAllError(str) {
  return true;
}

window.onerror = hideAllError;
$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  new Swiper('.m-welcome', {
    navigation: {
      nextEl: '.m-welcome .swiper-button-next',
      prevEl: '.m-welcome .swiper-button-prev'
    },
    pagination: {
      el: '.m-welcome .swiper-pagination'
    }
  });
  new Swiper('.m-home-reviews__cards', {
    navigation: {
      nextEl: '.m-home-reviews__cards .swiper-button-next',
      prevEl: '.m-home-reviews__cards .swiper-button-prev'
    }
  });
  new Swiper('.m-home-blog__cards', {
    navigation: {
      nextEl: '.m-home-blog .swiper-button-next',
      prevEl: '.m-home-blog .swiper-button-prev'
    },
    slidesPerView: 1,
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2
      },
      1024: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper('.m-papers__cards', {
    navigation: {
      nextEl: '.m-papers .swiper-button-next',
      prevEl: '.m-papers .swiper-button-prev'
    },
    slidesPerView: 1,
    spaceBetween: 30,
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 3
      }
    }
  });
  new Swiper('.m-seen__cards', {
    navigation: {
      nextEl: '.m-seen .swiper-button-next',
      prevEl: '.m-seen .swiper-button-prev'
    },
    slidesPerView: 1,
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2
      },
      1024: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper('.m-examples__cards', {
    navigation: {
      nextEl: '.m-examples .swiper-button-next',
      prevEl: '.m-examples .swiper-button-prev'
    },
    slidesPerView: 1,
    spaceBetween: 30,
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 3
      }
    }
  });
  $('.m-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.m-top').slideToggle('fast');
    $('.m-main').slideToggle('fast');
    $('.m-header__nav').slideToggle('fast');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').toggle();
  });
  $('.m-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'm-modal__centered') {
      $('.m-modal').toggle();
    }
  });
  $('.m-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').toggle();
  });
  $('.open-popup').on('click', function (e) {
    e.preventDefault();
    $('.m-popup__text').text($(this).data('what'));
    $('.m-popup__content').css('background-image', $(this).css('background-image'));
    $('.m-popup').toggle();
  });
  $('.m-popup__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'm-popup__centered') {
      $('.m-popup').toggle();
    }
  });
  $('.m-popup__close').on('click', function (e) {
    e.preventDefault();
    $('.m-popup').toggle();
  });
  $('.m-good__counter-plus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).next().val();

    if (currentVal < 99) {
      $(this).next().val(++currentVal);
    }
  });
  $('.m-good__counter-minus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).prev().val();

    if (currentVal > 1) {
      $(this).prev().val(--currentVal);
    }
  });
  $('.m-basket-card__counter-plus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).next().val();

    if (currentVal < 99) {
      $(this).next().val(++currentVal);
    }
  });
  $('.m-basket-card__counter-minus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).prev().val();

    if (currentVal > 1) {
      $(this).prev().val(--currentVal);
    }
  });
  $('.m-filter-collapse__header').on('click', function (e) {
    e.preventDefault();
    $(this).children().next().toggleClass('m-filter-collapse__icon_disabled');
    $(this).next().slideToggle('fast');
  });
  $('.m-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');
    $('.m-tabs__header li').removeClass('current');
    $('.m-tabs__content').removeClass('current');
    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });
  var galleryThumbs = new Swiper('.m-good__small', {
    spaceBetween: 28,
    slidesPerView: 3,
    direction: 'vertical',
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true
  });
  var galleryTop = new Swiper('.m-good__big', {
    thumbs: {
      swiper: galleryThumbs
    }
  });
});
//# sourceMappingURL=main.js.map
